<?php

use NestedJsonFlattener\Flattener\Flattener;

error_reporting(-1);

require 'vendor/autoload.php';
require 'config.php';


$ac = new ActiveCampaign($config['AC_URL'], $config['AC_KEY']);

$counter = 1;

while ($campaigns = getCampaigns($counter, $ac)) {
    
    unset($campaigns->result_code);
    unset($campaigns->result_message);
    unset($campaigns->result_output);
    unset($campaigns->http_code);
    unset($campaigns->success);
    
    foreach ($campaigns as $campaign) {
    
        unset($campaign->messages);
    
    }
    
    if (!$mastercampaigns) {
        $mastercampaigns = $campaigns;
    } else {
        $mastercampaigns = (object) array_merge((array) $campaigns, (array) $mastercampaigns);
    }
        
    $counter++;
       
}



$mastercampaigns = json_encode($mastercampaigns);

$flattener = new Flattener();

$flattener->setJsonData($mastercampaigns);
$flattener->writeCsv();


function getCampaigns( $counter = 1, $ac ) {
    
    $response = $ac->api("campaign/list?ids=all&page=$counter");
    
    if ($response->result_code == 1) {
        return $response;
    }
    
    return false;
    
}